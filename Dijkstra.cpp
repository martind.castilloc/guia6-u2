#include <fstream>
#include <iostream>

#include "Dijkstra.h"

using namespace std;

Dijkstra::Dijkstra() {}

void Dijkstra::crear_matriz (int cantidad_nodos, string nombres[]){

    this->matriz = new int*[cantidad_nodos];
    int distancia;

    /* Inicializamos la matriz */
    for (int i=0;i<cantidad_nodos;i++) {
        this->matriz[i] = new int[cantidad_nodos];
    }

    /* Llenar la matriz */
    for (int i=0;i<cantidad_nodos;i++) {
        cout << "\n";
        cout << "Trabajando con el nodo: " << nombres[i];
        cout << "\n";

        for (int j=0; j<cantidad_nodos;j++) {
            if (i != j) {
                cout << "ingrese la distancia al nodo <" << nombres[j] << ">: ";
                cin >> distancia;


                if(distancia == wctype("alpha")){
                    break;
                }
                this->matriz[i][j] = distancia;
            } else {
                /* Diagonal de la matriz */
                this->matriz[i][j] = 0;
            
            } 
        }
    }
}

void Dijkstra::nombre_nodos (int cantidad_nodos, string nombres[]){
    string nombre;

    for (int i=0;i<cantidad_nodos;i++) {
        cout << "\n";
        cout << "Ingrese el nombre del nodo " << i+1 << ": ";
        cin >> nombre;
        nombres[i] = nombre;
    }
}

// Copia contenido inicial a D[] desde la matriz M[][].
void Dijkstra::inicializar_vector_D (int D[], int cantidad_nodos) {

   for(int col = 0; col < cantidad_nodos; col++){
        D[col] = this -> matriz[0][col];
    }
}

// Inicializa con espacios el arreglo de caracteres.
void Dijkstra::inicializar_vector_caracter (char vector[], int cantidad_nodos) {
    int col;
  
    for (col=0; col<cantidad_nodos; col++) {
        vector[col] = ' ';
    }
}

// Aplica el algoritmo.
void Dijkstra::aplicar_dijkstra (char V[], char S[], char VS[], int D[], int cantidad_nodos) {
   int i;
    int v;
    // inicializar vector D[] segun datos de la matriz M[][]
    // estado inicial.
    inicializar_vector_D(D, cantidad_nodos);

    //
    cout << "\n----------Estados Iniciales----------\n" << endl;
    mostrar_matriz(cantidad_nodos);
    cout << endl;
    imprimir_vector_caracter(S, "S", cantidad_nodos);
    imprimir_vector_caracter(VS, "VS", cantidad_nodos);
    imprimir_vector_entero(D, cantidad_nodos);
    cout << "-------------------------------------\n\n";

    // agrega primer vertice.
    cout << "> agrega el primer valor V[0] a S[] y actualiza VS[]\n\n";
    agrega_vertice_a_S(S, V[0], cantidad_nodos);
    imprimir_vector_caracter(S, "S", cantidad_nodos);
    //
    actualizar_VS(V, S, VS, cantidad_nodos);
    imprimir_vector_caracter(VS, "VS", cantidad_nodos);
    imprimir_vector_entero(D, cantidad_nodos);
    
    for(i = 1; i < cantidad_nodos; i++){
        // se elige un vertice en v de VS[] tal que D[v] sea el minimo
        cout << "\n > elige vertice menor en VS[] segun valores en D[]\n";
        cout << "> lo agrega a S[] y actualiza VS[]\n";
        v = elegir_vertice(VS, D, V, cantidad_nodos);
        //
        agrega_vertice_a_S(S, v, cantidad_nodos);
        imprimir_vector_caracter(S, "S", cantidad_nodos);
        //
        actualizar_VS(V, S, VS, cantidad_nodos);
        imprimir_vector_caracter(VS, "VS", cantidad_nodos);
        //
        actualizar_pesos(D, VS, V, v, cantidad_nodos);
        imprimir_vector_entero(D, cantidad_nodos);
    }
}

// Esta funcion actualiza los valores del vector D llamando a la
// funcion calcular_minimo con los indices v y w.
void Dijkstra::actualizar_pesos(int D[], char VS[], char V[], char v, int cantidad_nodos){
    int i = 0;
    int indice_w, indice_v;
    
    cout << "\n> actualiza pesos en D[]\n";

    indice_v = buscar_indice_caracter(V, v, cantidad_nodos);
    while(VS[i] != ' '){
        if(VS[i] != v){
            indice_w = buscar_indice_caracter(V, VS[i], cantidad_nodos);
            D[indice_w] = calcular_minimo(D[indice_w], D[indice_v], this -> matriz[indice_v][indice_w]);
        }
        i++;
    }
}


int Dijkstra::calcular_minimo(int dw, int dv, int mvw) {
    int min = 0;

    if (dw == -1) {
        if (dv != -1 && mvw != -1)
        min = dv + mvw;
        else
        min = -1;

    } 
    else {
        if (dv != -1 && mvw != -1) {
            if (dw <= (dv + mvw)){
                min = dw;
            }
            else{
                min = (dv + mvw);
            }
        }
        else{
            min = dw;
        }
    }
    
    cout << "dw: " << dw << " dv: " << dv << " mvw: " << mvw << " min: " << min; 

    return min;
}


// Agrega vértice a S[].
void Dijkstra::agrega_vertice_a_S(char S[], char vertice, int cantidad_nodos) {
    int i;
  
    // Recorre buscando un espacio vacio.
    for (i=0; i<cantidad_nodos; i++) {
        if (S[i] == ' ') {
            S[i] = vertice;
            return;
        }
    }  
}

/* Elige vértice con menor peso en VS[].
 * Busca su peso en D[].*/
int Dijkstra::elegir_vertice(char VS[], int D[], char V[], int cantidad_nodos) {

	int i = 0;
	int menor = 0;
	int peso;
	int vertice;

	while (VS[i] != ' ') {
		peso = D[buscar_indice_caracter(V, VS[i], cantidad_nodos)];
    // Descarta valores infinitos (-1) y 0.
		if ((peso != -1) && (peso != 0)) {
			if (i == 0) {
				menor = peso;
				vertice = VS[i];
			} else {
				if (peso < menor) {
					menor = peso;
					vertice = VS[i];
				}
			}
		}

		i++;
	}
  
	cout<<"\nvertice: "<<vertice<<endl<<endl;
	return vertice;
}

/* Función para mostrar la matriz */
void Dijkstra::mostrar_matriz(int cantidad_nodos) {
    for (int i=0; i<cantidad_nodos;i++) {
        for (int j=0;j<cantidad_nodos;j++) {
            cout << this->matriz[i][j] << " ";
        }
        cout << "\n";
    }
}

// Retorna el índice del caracter consultado.
int Dijkstra::buscar_indice_caracter(char V[], char caracter, int N) {
    int i;
    
    for (i=0; i<N; i++) {
        if (V[i] == caracter)
        return i;
    }
    
    return i;
}

// Busca la aparición de un caracter en un vector de caracteres.
int Dijkstra::busca_caracter(char c, char vector[], int cantidad_nodos) {
	int j;
  
	for (j=0; j<cantidad_nodos; j++) {
		if (c == vector[j]) {
		return true;
		}
	}
	
	return false;
}

void Dijkstra::alg_dijkstra(int cantidad_nodos){
    // Inicializa los vectores con el tamaño indicado
    int D[cantidad_nodos];
    char V[cantidad_nodos];
    char S[cantidad_nodos];
    char VS[cantidad_nodos];
    
    inicializar_vector_caracter(V, cantidad_nodos);
    inicializar_vector_caracter(S, cantidad_nodos);
    inicializar_vector_caracter(VS, cantidad_nodos);
    leer_nodos(V, cantidad_nodos);
    aplicar_dijkstra(V, S, VS, D, cantidad_nodos);
}

// Actualiza VS[] cada ves que se agrega un elemnto a S[].
void Dijkstra::actualizar_VS(char V[], char S[], char VS[], int cantidad_nodos) {
    int k = 0;
  
    inicializar_vector_caracter(VS, cantidad_nodos);
  
    for (int j=0; j<cantidad_nodos; j++){
        // Por cada caracter de V[] evalua si está en S[],
        // Sino está, lo agrega a VS[].
        if (busca_caracter(V[j], S, cantidad_nodos) != true) {
            VS[k] = V[j];
            k++;
        }
    }
}


/* Lee datos de los nodos.
 * Inicializa utilizando código ASCII.*/
void Dijkstra::leer_nodos (char vector[], int cantidad_nodos) {
    int i;
    int inicio = 97;
  
    for (i=0; i<cantidad_nodos; i++) {
        vector[i] = inicio+i;
    }
}


void Dijkstra::imprimir_vector_caracter(char vector[], char *nomVector, int cantidad_nodos) {
    int i;
    
    for (i=0; i<cantidad_nodos; i++) {
        cout << nomVector << "[" << i << "]" << ": " << vector[i];
    }

    cout << endl;
}

// Imprime el contenido de un vector de caracteres.
void Dijkstra::imprimir_vector_entero(int vector[], int cantidad_nodos) {
    int i;
    
    for (i=0; i<cantidad_nodos; i++) {
        cout << "D[" << i << "]" << ": " << vector[i];
    }

    cout << endl;
}



/* Genera y muestra apartir de una matriz bidimensional de enteros,
 el grafo correspondiente.*/
void Dijkstra::imprimir_grafo(int cantidad_nodos, string nombres[]) {

    ofstream fp;
            
    /* Abre archivo */
    fp.open ("grafo.txt");
    fp << "digraph G {" << endl;
    fp << "graph [rankdir=LR]" << endl;
    fp << "node [style=filled fillcolor=yellow];" << endl;
    
    // Escribir en el archivo la matriz
    for (int i=0; i<cantidad_nodos; i++) {
        for (int j=0; j<cantidad_nodos; j++) {
            // evalua la diagonal principal.
            if (i != j) {
                if (matriz[i][j] > 0) {
                    fp << "\n" << '"' << nombres[i] << '"' << "->" << '"' << nombres[j] << '"' << " [label=" << this->matriz[i][j] << "];";
                }
            }
        }
    }
    fp << "}" << endl;

    /* Cierra archivo */
    fp.close();
                    
    /* Genera el grafo */
    system("dot -Tpng -ografo.png grafo.txt &");
            
    /* Visualiza el grafo */ 
    system("eog grafo.png &");
}

// Función para limpiar la terminal, no afecta en el problema.
void Dijkstra::clear() {
    cout << "\x1B[2J\x1B[H";
}