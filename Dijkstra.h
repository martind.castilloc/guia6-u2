#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include <iostream>
#include <fstream>

using namespace std;

class Dijkstra{
    private:
        int **matriz;
    public:
        // Constructor
        Dijkstra();

        void nombre_nodos(int cantidad_nodos, string nombres[]);
        void leer_nodos(char vector[], int cantidad_nodos);      
        // Copia contenido inicial a D[] desde la matriz M[][].
        void inicializar_vector_D(int D[], int cantidad_nodos);
        // Inicializa con espacios el arreglo de caracteres.
        void inicializar_vector_caracter(char vector[], int cantidad_nodos);
        // Aplica el algoritmo
        void aplicar_dijkstra(char V[], char S[], char VS[], int D[], int cantidad_nodos);
        // Actualiza VS[] cada ves que se agrega un elemnto a S[].
        void actualizar_VS(char V[], char S[], char VS[], int cantidad_nodos);
        int buscar_indice_caracter(char V[], char caracter, int N);      
        int elegir_vertice(char VS[], int D[], char V[], int cantidad_nodos); 
        void agrega_vertice_a_S(char S[], char vertice, int cantidad_nodos);     
        void mostrar_matriz(int cantidad_nodos);
        void alg_dijkstra(int cantidad_nodos);
        void actualizar_pesos (int D[], char VS[], char V[], char v, int cantidad_nodos);
        int calcular_minimo(int dw, int dv, int mvw);
        int busca_caracter(char c, char vector[], int cantidad_nodos);
        // Crea la mtriz
        void crear_matriz(int cantidad_nodo, string nombres[]);
        // Funcion para limpiar la terminal.
        void clear();

        // Impresiones
        void imprimir_vector_caracter(char vector[], char *, int cantidad_nodos);
        void imprimir_vector_entero(int vector[], int cantidad_nodos);
        void imprimir_grafo(int cantidad_nodos, string nombres[]);


};
#endif