#include <fstream>
#include <iostream>

using namespace std;

#include "Dijkstra.h"

//Función para el menu. 
string menu (string opc) {

    cout << "\n";
    cout << "\t     ---------------------------- " << endl;
	cout << "\t     ***** MENÚ DE OPCIONES ***** " << endl;
	cout << "\t     ---------------------------- " << endl;
	cout << "\n  [1]  > Aplicar algoritmo Dijkstra\t" << endl;
	cout << "  [2]  > Mostrar matriz\t" << endl;
    cout << "  [3]  > Visualizar el grafo \t" << endl;
    cout << "  [4]  > Salir del programa\t" << endl;
    cout << "Opción: ";

    cin >> opc;
	cin.ignore();

    return opc;
}

// Funcion principal del programa
int main(int argc, char **argv) {
    // Valida cantidad de parámetros mínimos.
    if (argc<2) {
		cout << "Recuerde que el programa recibe dos parametros de entrada "<<endl;
    }

    // Convierte string a entero && tamano_matriz es una variable int para crear la matriz NxN.
    int cantidad_nodos = atoi(argv[1]);

    // Inicializa las variables para el menu
    string option = "\0";
    string dato = "\0";

    // Crea el objeto dijkstra
    Dijkstra *dijkstra = new Dijkstra();

    // Inicializa un Array del tipo string con nombres de los nodos
    string nombres[cantidad_nodos];

    // Genera la matriz como atributo del objeto dijkstra junto a los nombres y el tamaño
    dijkstra->nombre_nodos(cantidad_nodos, nombres);
    dijkstra->crear_matriz(cantidad_nodos, nombres);

    dijkstra->clear();

    while (option != "4") {

        option = menu(option);

        // Opción para ver los caminos más cortos de un nodo específico
        if (option == "1") {
            dijkstra->alg_dijkstra(cantidad_nodos);
        
        // Opción para mostrar la matriz
        }else if (option == "2") {
            dijkstra->mostrar_matriz(cantidad_nodos);

        //Opción para crear y visualizar el grafo con la librería graphviz
        }else if (option == "3") {
            dijkstra->imprimir_grafo(cantidad_nodos, nombres);

        }
    }

    return 0;
}
